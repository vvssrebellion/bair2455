package inventory.service;

import inventory.exceptions.PartException;
import inventory.exceptions.ProductException;
import inventory.model.*;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

import java.util.List;

public class InventoryService {

    private InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        String errorMessage = Part.isValidPart(inhousePart);
        if(!errorMessage.equals("")){
            throw new PartException(errorMessage);
        }
        repo.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        String errorMessage = Part.isValidPart(outsourcedPart);
        if(!errorMessage.equals("")){
            throw new PartException(errorMessage);
        }
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        String errorMessage = Product.isValidProduct(product);
        if(!errorMessage.equals("")){
            throw new ProductException(errorMessage);
        }
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part searchPart(String search) {
        return repo.searchPart(search);
    }

    public Product searchProduct(String search) {
        return repo.searchProduct(search);
    }

    public void updateInhousePart(int partIndex, InhousePart inhousePart){
        String errorMessage = Part.isValidPart(inhousePart);
        if(!errorMessage.equals("")){
            throw new PartException(errorMessage);
        }
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, OutsourcedPart outsourcedPart){
        String errorMessage = Part.isValidPart(outsourcedPart);
        if(!errorMessage.equals("")){
            throw new PartException(errorMessage);
        }
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex,Product product){
        String errorMessage = Product.isValidProduct(product);
        if(!errorMessage.equals("")){
            throw new ProductException(errorMessage);
        }
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

    public boolean verif30SamePrice(Product product){
        if(product.getInStock()<100){
            return false;
        }
        double inc = 0;
        List<Product> products = getAllProducts();
        for(Product p : products){
            if(p.getPrice() == product.getPrice()){
                inc=inc+p.getInStock();
            }
        }
        if(inc < 30) {
            return false;
        }
        return true;
    }

}
