
package inventory.model;


public abstract class Part {

    // Declare fields
    private int partId;
    private String name;
    private double price;
    private int inStock;
    private int min;
    private int max;
    
    // Constructor
    protected Part(int partId, String name, double price, int inStock, int min, int max) {
        this.partId = partId;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.min = min;
        this.max = max;
    }
    
    // Getters
    public int getPartId() {
        return partId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getInStock() {
        return inStock;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
    
    // Setters
    public void setPartId(int partId) {
        this.partId = partId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }
    
    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @return 
     */
    public static String isValidPart(Part part) {
        String errorMessage = "";
        if(part.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(part.getName().length()<3) {
            errorMessage += "A name must have the length greater then 2 ";
        }
        if(part.getName().length()>99) {
            errorMessage += "A name must have the length smaller the 100";
        }
        if(part.getPrice()< 5) {
            errorMessage += "The price must be greater than 5. ";
        }
        if(part.getPrice()> 999) {
            errorMessage += "The price must be smaller than 1000. ";
        }
        if(part.getInStock() < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(part.getInStock() > 999999) {
            errorMessage += "Inventory level must be smaller than 1000000. ";
        }
        if(part.getMin() > part.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(part.getInStock() < part.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(part.getInStock() > part.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        return errorMessage;
    }

    @Override
    public String toString() {
        return this.partId+","+this.name+","+this.price+","+this.inStock+","+
                this.min+","+this.max;
    }
}
