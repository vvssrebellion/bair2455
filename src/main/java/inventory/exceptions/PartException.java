package inventory.exceptions;

public class PartException extends RuntimeException {
    public PartException(String message) {
        super(message);
    }
}