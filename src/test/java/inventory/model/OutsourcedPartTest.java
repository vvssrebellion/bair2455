package inventory.model;

import inventory.model.OutsourcedPart;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OutsourcedPartTest {
    private OutsourcedPart part;

    @BeforeEach
    void setUp() {
        part = new OutsourcedPart(0,"testPart",7,50,1,100,"company");

    }

    @AfterEach
    void tearDown() {
        part = null;
    }

    @Test
    void getCompanyName() {
        assertEquals("company",part.getCompanyName());
    }

    @Test
    void setCompanyName() {
        part.setCompanyName("newCompany");
        assertEquals("newCompany",part.getCompanyName());
    }
}
