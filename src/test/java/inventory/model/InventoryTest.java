package inventory.model;

import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
    }

    @AfterEach
    void tearDown() {
        inventory = null;
    }

    @Test
    void addPart() {
        OutsourcedPart part = new OutsourcedPart(1, "testPart", 7, 50, 1, 100, "company");
        inventory.addPart(part);
        assertEquals(1,inventory.getAllParts().size());
    }

    @Test
    void deletePart() {
        OutsourcedPart part = new OutsourcedPart(1, "testPart", 9, 60, 1, 100, "company");
        inventory.addPart(part);
        assertEquals(1,inventory.getAllParts().size() );
        inventory.deletePart(part);
        assertEquals(0,inventory.getAllParts().size());
    }

    @Test
    void addProduct(){
        ObservableList<Part> parts = null;
        Product product = new Product(1,"testProduct",11,70,1,100,parts);
        inventory.addProduct(product);
        assertEquals(1,inventory.getProducts().size());
    }

    @Test
    void deleteProduct(){
        ObservableList<Part> parts = null;
        Product product = new Product(1,"testProduct",13,80,1,100,parts);
        inventory.addProduct(product);
        assertEquals(1,inventory.getProducts().size());
        inventory.removeProduct(product);
        assertEquals(0,inventory.getProducts().size());
    }
}
