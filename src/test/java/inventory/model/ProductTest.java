package inventory.model;

import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {
    private Product product;
    ObservableList<Part> parts;

    @BeforeEach
    void setUp() {
        product = new Product(1, "product", 10, 50, 1, 100, parts);
    }

    @AfterEach
    void tearDown() {
        product = null;
    }

    @Test
    void getPrice() {
        assertEquals(10,product.getPrice());
    }

    @Test
    void setPrice() {
        product.setPrice(10);
        assertEquals(10,product.getPrice());
    }

    @Test
    void getName(){
        assertEquals("product",product.getName());
    }

    @Test
    void setName(){
        product.setName("testProduct");
        assertEquals("testProduct",product.getName());
    }
}
