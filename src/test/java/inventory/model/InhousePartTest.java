package inventory.model;

import inventory.model.InhousePart;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InhousePartTest {
    private InhousePart part;

    @BeforeEach
    void setUp(){
        part = new InhousePart(1, "testPart", 20, 30, 1, 50, 9);
    }

    @AfterEach
    void tearDown(){
        part = null;
    }

    @Test
    void getMachineId() {
        assertEquals(9,part.getMachineId());
    }

    @Test
    void setMachineId() {
        part.setMachineId(7);
        assertEquals(7,part.getMachineId());
    }
}
