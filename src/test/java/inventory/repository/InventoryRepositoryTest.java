package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

public class InventoryRepositoryTest {
    private InventoryRepository repo;
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        repo = new InventoryRepository("data/mockTest.txt");
        repo.setInventory(inventory);
    }

    @AfterEach
    void tearDown() {
        inventory = null;
        repo = null;
    }

    @Test
    void addPartAndProduct() {
        Part part = new InhousePart(2,"inhousePart1",10,20,10,100,1);

        ObservableList<Part> list = FXCollections.observableArrayList();
        list.add(part);
        Product product=new Product(1,"product",100,50,20,100,list);
        ObservableList<Product> listProducts = FXCollections.observableArrayList();
        listProducts.add(product);

        Mockito.doNothing().when(inventory).addProduct(product);
        Mockito.doNothing().when(inventory).addPart(part);

        Mockito.when(inventory.getAllParts()).thenReturn(list);
        Mockito.when(inventory.getProducts()).thenReturn(listProducts);

        Mockito.verify(inventory, times(0)).addPart(part);

        repo.addPart(part);

        Mockito.verify(inventory, times(1)).addPart(part);
        Mockito.verify(inventory, times(1)).getAllParts();

    }

    @Test
    void deletePart() {
        Part part = new InhousePart(2,"inhousePart1",10,20,10,100,1);



        ObservableList<Part> list = FXCollections.observableArrayList();
        Product product=new Product(1,"product",100,50,20,100,list);
        ObservableList<Product> listProducts = FXCollections.observableArrayList();

        Mockito.doNothing().when(inventory).addProduct(product);
        Mockito.doNothing().when(inventory).addPart(part);

        Mockito.when(inventory.getAllParts()).thenReturn(list);
        Mockito.when(inventory.getProducts()).thenReturn(listProducts);

        Mockito.verify(inventory, times(0)).addPart(part);
        Mockito.verify(inventory, times(0)).addProduct(product);
        Mockito.verify(inventory, times(0)).deletePart(part);

        repo.addPart(part);
        repo.deletePart(part);

        Mockito.verify(inventory, times(1)).addPart(part);
        Mockito.verify(inventory, times(1)).deletePart(part);
        Mockito.verify(inventory, times(2)).getAllParts();

    }
}
