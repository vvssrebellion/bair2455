package inventory.service;

import inventory.exceptions.PartException;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InventoryServiceTest {
    private static InventoryRepository repo;
    private static InventoryService service;
    @BeforeAll
    static void  setUp() {
        repo=new InventoryRepository("data/items.txt");
        service = new InventoryService(repo);
    }


    @DisplayName("valid data")
    @Tag("ECP")
    @Order(1)
    @Test
    void ECP_Valid(){
        //arrange
        String name="part1";
        double price=10.99;
        int inStock=100;
        int min=10;
        int max=999;
        int partDynamicValue=10;
        int oldSize=service.getAllParts().size();
        Part part;
        //act
        try{
            service.addInhousePart(name,price,inStock,min,max,partDynamicValue);
        }
        catch (PartException pe){
            fail();
        }


        //assert
        try {
            int newSize = service.getAllParts().size();
            assertEquals(oldSize + 1, newSize);
            part = service.getAllParts().get(newSize - 1);
            assertEquals(part.getName(), name);
            assertEquals(part.getPrice(), price);
            assertEquals(part.getInStock(), inStock);
        } catch (Exception e){
            fail();
        }

    }

    @ParameterizedTest
    @DisplayName("Non-valid price and inStock")
    @Tag("ECP")
    @Order(2)
    @ValueSource(ints = {3,150})
    void ECP_NonValid(int price){
        //arrange
        String name="part2";
        int inStock=-5;
        int min=10;
        int max=999;
        int partDynamicValue=10;
        int oldSize=service.getAllParts().size();
        Part part;

        //act
        assertThrows(PartException.class,()->{
            service.addInhousePart(name,price,inStock,min,max,partDynamicValue);
        });



        //assert
        int newSize = service.getAllParts().size();
        assertEquals(oldSize, newSize);

    }

    @ParameterizedTest
    @DisplayName("Valid data")
    @Tag("BVA")
    @Order(3)
    @ValueSource(ints = {6,999})
    void BVA_Valid(int price){
        //arrange
        String name="part3";
        int inStock=1;
        int min=1;
        int max=1000000;
        int partDynamicValue=10;
        int oldSize=service.getAllParts().size();
        Part part;

        try{
            service.addInhousePart(name,price,inStock,min,max,partDynamicValue);
        }
        catch (PartException pe){
            fail();
        }


        //assert
        try {
            int newSize = service.getAllParts().size();
            assertEquals(oldSize + 1, newSize);
            part = service.getAllParts().get(newSize - 1);
            assertEquals(part.getName(), name);
            assertEquals(part.getPrice(), price);
            assertEquals(part.getInStock(), inStock);
        } catch (Exception e){
            fail();
        }

    }

    @ParameterizedTest
    @DisplayName("Non-valid price and inStock")
    @Tag("BVA")
    @Order(4)
    @ValueSource(ints = {5,1000})
    void BVA_NonValid(int price){
        //arrange
        String name="part4";
        int inStock1=0;
        int inStock2=1000000;
        int min=10;
        int max=999;
        int partDynamicValue=10;
        int oldSize=service.getAllParts().size();
        Part part;

        //act
        assertThrows(PartException.class,()->{
            service.addInhousePart(name,price,inStock1,min,max,partDynamicValue);

        });
        assertThrows(PartException.class,()->{
            service.addInhousePart(name,price,inStock2,min,max,partDynamicValue);
        });




        //assert
        int newSize = service.getAllParts().size();
        assertEquals(oldSize, newSize);

    }

    @DisplayName("Valid30")
    @Tag("Lab3")
    @Order(5)
    @Test
    void testVerif30Valid(){

        Product product=new Product(99,"testName",11.0,200,5,240,null);
        assertTrue(service.verif30SamePrice(product));
    }

    @DisplayName("InValid30")
    @Order(5)
    @Tag("Lab3")
    @Test
    void testVerif30InValidInStock(){

        Product product=new Product(98,"testName90",11.0,90,5,240,null);
        assertFalse(service.verif30SamePrice(product));
    }

    @DisplayName("InValid30")
    @Order(5)
    @Tag("Lab3")
    @Test
    void testVerif30InValidPrice(){

        Product product=new Product(97,"testName",10.0,200,5,240,null);
        assertFalse(service.verif30SamePrice(product));
    }

    @AfterAll
    static void tearDown() {
    }
}