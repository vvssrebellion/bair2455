package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryServiceMockitoTest {
    private InventoryRepository repo;
    private InventoryService service;

    @BeforeEach
    void setUp() {
        repo = mock(InventoryRepository.class);
        service = new InventoryService(repo);
    }

    @AfterEach
    void tearDown() {
        repo = null;
        service = null;
    }

    @Test
    void searchPart() {
        OutsourcedPart part = new OutsourcedPart(1, "testPart", 10, 50, 1, 100, "company");
        ObservableList<Part> list = FXCollections.observableArrayList();
        list.add(part);
        Mockito.when(repo.searchPart("testPart")).thenReturn(part);
        Mockito.when(repo.getAutoPartId()).thenReturn(1);

        service.addOutsourcePart("testPart", 30, 70, 1, 100, "company");

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(0)).searchPart("testPart");

        assertEquals(service.searchPart("testPart"), part);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(1)).searchPart("testPart");
    }

    @Test
    void deletePart() {
        OutsourcedPart part = new OutsourcedPart(1, "testPart", 10, 50, 1, 100, "company");
        ObservableList<Part> list = FXCollections.observableArrayList();

        Mockito.when(repo.getAutoPartId()).thenReturn(7);
        Mockito.when(repo.getAllParts()).thenReturn(list);
        Mockito.doNothing().when(repo).deletePart(part);

        service.addOutsourcePart("testPart", 30, 70, 1, 100, "company");

        Mockito.verify(repo, times(0)).deletePart(part);
        service.deletePart(part);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(0)).getAllParts();
        Mockito.verify(repo, times(1)).deletePart(part);

        assertEquals(0,service.getAllParts().size());

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(1)).getAllParts();
    }
}