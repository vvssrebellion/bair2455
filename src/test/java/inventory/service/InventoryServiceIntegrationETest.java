package inventory.service;

import java.io.FileWriter;
import java.io.IOException;

import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class InventoryServiceIntegrationETest {
    private InventoryService service;
    private InventoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository("data/mockTest.txt");
        service = new InventoryService(repository);
    }

    @AfterEach
    void tearDown() throws IOException {
        repository = null;
        service = null;
        new FileWriter("src\\main\\resources\\data\\mockTest.txt", false).close();
    }

    @Test
    void addOutsourcedPart() {
        service.addOutsourcePart("testPart",7,50,1,100,"company");
        assertEquals(1, service.getAllParts().size());
    }

    @Test
    void updateOutsourcePart(){
        service.addOutsourcePart("testPart",7,50,1,100,"company");
        assertEquals(2, service.getAllParts().size());
        OutsourcedPart part = new OutsourcedPart(0,"newPart",7,50,1,100,"company");
        service.updateOutsourcedPart(0, part);
        assertEquals(0,service.searchPart("newPart").getPartId());
    }
}
