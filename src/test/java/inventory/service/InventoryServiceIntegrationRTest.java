package inventory.service;

import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceIntegrationRTest {
    private InventoryService service;
    private InventoryRepository repository;
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        repository = new InventoryRepository("data/mockTest.txt");
        repository.setInventory(inventory);
        service = new InventoryService(repository);
    }

    @AfterEach
    void tearDown() {
        inventory = null;
        repository = null;
        service = null;
    }

    @Test
    void addOutsourcedPart() {
        OutsourcedPart part = new OutsourcedPart(0,"testPart",7,50,1,100,"company");
        ObservableList<Part> listParts = FXCollections.observableArrayList();
        listParts.add(part);
        Product product = new Product(1,"testProduct",10,50,1,100,listParts);
        ObservableList<Product> listProducts = FXCollections.observableArrayList();
        listProducts.add(product);

        Mockito.when(inventory.searchPart("testPart")).thenReturn(part);
        Mockito.when(inventory.getAllParts()).thenReturn(listParts);
        Mockito.when(inventory.getProducts()).thenReturn(listProducts);

        service.addOutsourcePart("testPart", 9, 60, 1, 100, "company");

        Mockito.verify(inventory, times(0)).searchPart("testPart");

        assertEquals(0, service.searchPart("testPart").getPartId());

        Mockito.verify(inventory, times(1)).searchPart("testPart");
    }

    @Test
    void deletePart() {
        OutsourcedPart part = new OutsourcedPart(0,"testPart",7,50,1,100,"company");
        ObservableList<Part> listParts = FXCollections.observableArrayList();
        listParts.add(part);
        Product product = new Product(1,"testProduct",10,50,1,100,listParts);
        ObservableList<Product> listProducts = FXCollections.observableArrayList();
        listProducts.add(product);

        Mockito.when(inventory.searchPart("testPart")).thenReturn(null);
        Mockito.doNothing().when(inventory).deletePart(part);
        Mockito.when(inventory.getAllParts()).thenReturn(listParts);
        Mockito.when(inventory.getProducts()).thenReturn(listProducts);

        service.addOutsourcePart("testPart", 9, 60, 1, 100, "company");

        Mockito.verify(inventory, times(0)).deletePart(part);

        service.deletePart(part);

        Mockito.verify(inventory, times(1)).deletePart(part);

        Mockito.verify(inventory, times(0)).searchPart("testPart");

        assertNull(service.searchPart("testPart"));

        Mockito.verify(inventory, times(1)).searchPart("testPart");

    }
}
